require('./bootstrap');
window.Vue = require('vue');
window.faker = require('faker');

import Vue          from "vue";
import App          from "./App.vue";
import VueRouter    from 'vue-router';
import {app_routes} from './routes';
import Buefy        from 'buefy'
import 'buefy/dist/buefy.css'
import { library } from '@fortawesome/fontawesome-svg-core';
// internal icons
import { faCheck, faCheckCircle, faInfoCircle, faExclamationTriangle, faExclamationCircle,
    faArrowUp, faAngleRight, faAngleLeft, faAngleDown,
    faEye, faEyeSlash, faCaretDown, faCaretUp, faUpload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faCheck, faCheckCircle, faInfoCircle, faExclamationTriangle, faExclamationCircle,
    faArrowUp, faAngleRight, faAngleLeft, faAngleDown,
    faEye, faEyeSlash, faCaretDown, faCaretUp, faUpload);
Vue.component('vue-fontawesome', FontAwesomeIcon);

Vue.use(Buefy, {
    defaultIconComponent: 'vue-fontawesome',
    defaultIconPack: 'fas',
})
Vue.use(VueRouter);


const router = new VueRouter({
    mode  : 'history',
    routes: app_routes,
});

const app = new Vue({
    el        : '#app',
    router,
    components: {'app': App},
});

