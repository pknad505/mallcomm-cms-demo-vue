import Dashboard from './pages/Dashboard';
import People from './pages/People';

export const app_routes = [
    {path: '/dashboard', component: Dashboard, name: 'Dashboard', alias: ['/home', '/']},
    {path: '/people', component: People, name: 'People', alias: ['/people-all']},
]

